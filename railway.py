
#Priority queue for distances between towns
#(sorted smallest distance -> largest
class DistanceList :

    def __init__(self) :
        self.distances = []

    def addDistance(self, town, distance) :
        
        for i in range(0, len(self.distances)) :
            (t, d) = self.distances[i]
            if distance < d :
                self.distances.insert(i, (town, distance))
                return
        
        self.distances.append((town, distance))

    def pop(self) :
        return self.distances.pop(0)

    def empty(self) :
        return len(self.distances) == 0

    def update(self, town, distance) :
        
        for i in range(0, len(self.distances)) :
            (t, d) = self.distances[i]
            if(t == town) :
                self.distances.remove((t,d))
                self.addDistance(town, distance)
    
    def printDistances(self) :
        print self.distances


class RailGraph :

    def __init__(self, connections) :
        self.towns = {}

        for c in connections :
            self.addConnection(c)


    def addConnection(self, connection):
        #connection is a string with first 2 characters being the town names,
        #the remaining chars are the distance.  
        # Ex: 'AB7'

        fromTown = connection[0]
        toTown = connection[1]
        distance = int(connection[2:])

        #if there is a new town, add to the towns map
        if(fromTown not in self.towns) :
            self.towns[fromTown] = {}
        if(toTown not in self.towns) :
            self.towns[toTown] = {}

        self.towns[fromTown][toTown] = distance


    def recFixedRouteDistance(self, route) :
        fromTown = route[0];
        nextTown = route[1];
        
        distance = float("inf")
        if(nextTown in self.towns[fromTown]) :
            distance = self.towns[fromTown][nextTown]
        else :
            return distance

        if(len(route) > 2) :
            return distance + self.recFixedRouteDistance(route[1:])

        return distance

    
    def fixedRouteDistance(self, route) :

        distance = self.recFixedRouteDistance(route)
        return str(distance) if distance != float("inf") else "NO SUCH ROUTE"
        

    def routeCountForMaxStops(self, fromTown, toTown, stops) :

        count = 0
        adjacentTowns = self.towns[fromTown]
        for t in adjacentTowns.keys() :
            if(t == toTown) :
                count += 1
            if(stops > 1) :
                count += self.routeCountForMaxStops(t, toTown, stops - 1)
        
        return count


    def routeCountForExactStops(self, fromTown, toTown, stops) :

        count = 0
        adjacentTowns = self.towns[fromTown]
        for t in adjacentTowns.keys() :
            if(t == toTown and stops == 1) :
                count += 1
            if(stops > 1) :
                count += self.routeCountForExactStops(t, toTown, stops - 1)
        
        return count


    def routeCountForDistance(self, fromTown, toTown, maxDistance, distance = 0) :

        count = 0
        adjacentTowns = self.towns[fromTown]
        for t in adjacentTowns.keys() :
            newDistance = distance + adjacentTowns[t]
            if(t == toTown and newDistance < maxDistance) :
                count += 1
            if(newDistance < maxDistance) :
                count += self.routeCountForDistance(t, toTown, maxDistance, newDistance)

        return count
        

    def shortestDistance(self, fromTown, toTown):
        
        unvisited = DistanceList() #queue of distances to towns ordered smallest to greatest
        bestDistances = {} #hashmap of best distances to avoid searching 'unvisited' queue

        for t in self.towns.keys() :

            #assign starting town to infinite distance instead of zero
            #so that we can get distance to and from same town for question #9
            if(t == fromTown) :
                bestDistances[t] = float("inf") 
                unvisited.addDistance(t,float("inf")) 
                continue
            
            if(t in self.towns[fromTown].keys()) :
                bestDistances[t] = self.towns[fromTown][t]
                unvisited.addDistance(t, self.towns[fromTown][t])
            else :
                bestDistances[t] = float("inf")
                unvisited.addDistance(t, float("inf"))
        
        while(not unvisited.empty()) :

            (town, distance) = unvisited.pop()

            if(town == toTown) :
                return str(distance)

            adjacentTowns = self.towns[town]

            for t in adjacentTowns.keys() :
                distance = distance + self.towns[town][t] 
                if(distance < bestDistances[t]) :
                    bestDistances[t] = distance
                    unvisited.update(t, distance)

        return "NO SUCH ROUTE"
        

def main():

    print "RailGraphTest"

    graph = RailGraph(["AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7"])

    print "#1: " + graph.fixedRouteDistance("ABC")
    print "#2: " + graph.fixedRouteDistance("AD")
    print "#3: " + graph.fixedRouteDistance("ADC")
    print "#4: " + graph.fixedRouteDistance("AEBCD")
    print "#5: " + graph.fixedRouteDistance("AED")

    print "#6: " + str(graph.routeCountForMaxStops("C", "C", 3))
    print "#7: " + str(graph.routeCountForExactStops("A", "C", 4))

    print "#8: " + graph.shortestDistance("A", "C")
    print "#9: " + graph.shortestDistance("B", "B")

    print "#10: " + str(graph.routeCountForDistance("C", "C", 30))

if __name__ == "__main__":
    main()
